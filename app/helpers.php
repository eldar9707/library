<?php
function auth_user()
{
    if (session()->exists('libraryId'))
    {
        return App\Models\User::where('libraryId', session()->get('libraryId'))->first();
    } else {
        return NULL;
    }
}
