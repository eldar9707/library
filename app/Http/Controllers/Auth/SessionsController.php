<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\SessionRequest;
use App\Models\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class SessionsController extends AuthController
{

    /**
     * @param Request $request
     * @return Application|Factory|View|RedirectResponse
     */
    public function create(Request $request)
    {
        if($request->session()->exists('libraryId')) {
            return redirect()->route('books.index')->with('error', 'You are already login!');
        }
        return view('sessions.create');
    }

    /**
     * @param SessionRequest $request
     * @return RedirectResponse
     */
    public function store(SessionRequest $request): RedirectResponse
    {
        $user_collection = User::where('libraryId', $request->get('libraryId'))->get();
        if ($user_collection->isNotEmpty()) {
            $user = $user_collection->first();
            if ($this->auth($user, $request->get('password'))) {
                $this->logIn($user);
                return redirect()->route('books.index')->with('success', 'You are login success');
            }
        }
        return redirect()->back()->with('error', 'Incorrect email or password');
    }

    /**
     * @return RedirectResponse
     */
    public function destroy(): RedirectResponse
    {
        $this->logOut();
        return redirect()->route('books.index')->with('success', 'You are success logout');
    }
}
