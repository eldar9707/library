<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    /**
     * @param User $user
     */
    protected function logIn(User $user): void
    {
        session()->put('libraryId', $user->libraryId);
    }

    /**
     * @param User $user
     * @param string $password
     * @return bool
     */
    protected function auth(User $user, string $password): bool
    {
        return Hash::check($password, $user->password);
    }

    /**
     *
     */
    protected function logOut(): void
    {
        session()->remove('libraryId');
        session()->regenerate();
    }
}
