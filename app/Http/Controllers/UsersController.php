<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegisterRequest;
use App\Models\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UsersController extends \App\Http\Controllers\Auth\AuthController
{
    /**
     * @param Request $request
     * @return Application|Factory|View|RedirectResponse
     */
    public function register(Request $request)
    {
        if ($request->session()->exists('libraryId')) {
            return redirect()->route('books.index')->with('error', 'You are already registered!');
        }
        return view('users.register');
    }

    /**
     * @param RegisterRequest $request
     * @return RedirectResponse
     */
    public function store(RegisterRequest $request): RedirectResponse
    {
        $payload = collect($request->all());
        $payload['password'] = Hash::make($payload->get('password'));
        $user = User::create($payload->all());
        $this->logIn($user);
        return redirect()->route('books.index')->with('success', "Your library card ID: {$payload['libraryId']}");
    }

    /**
     * @return Application|Factory|View
     */
    public function show()
    {
        $applications = auth_user()->applications->all();

        return view('users.books', compact('applications'));
    }
}
