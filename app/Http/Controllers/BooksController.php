<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Models\Category;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;

class BooksController extends Controller
{
    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $categories = Category::all();
        $books = Book::paginate(9);
        return view('books.index', compact('books', 'categories'));
    }

    /**
     * @param $id
     * @return Application|Factory|View
     */
    public function category($id)
    {
        $genre = Category::findOrFail($id);
        return view('books.category', compact('genre'));
    }
}
