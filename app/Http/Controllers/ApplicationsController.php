<?php

namespace App\Http\Controllers;

use App\Models\Application;
use App\Models\Book;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class ApplicationsController extends Controller
{
    /**
     * @param Book $book
     * @return \Illuminate\Contracts\Foundation\Application|Factory|View
     */
    public function create(Book $book)
    {
        $user_id = auth_user()->id;
        $libraryId = auth_user()->libraryId;
        return view('books.application', compact('book', 'user_id', 'libraryId'));
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        $libraryId = auth_user()->libraryId;
        $request->validate([
            'libraryId' => ['required', Rule::in($libraryId)],
            'return_date' => 'bail|required|date|after:now'
        ]);

        $application = new Application();
        $application->libraryId = $libraryId;
        $application->book_id = $request->input('book_id');
        $application->user()->associate(auth_user());
        $application->return_date = $request->input('return_date');
        $application->save();
        $book = Book::find($request->input('book_id'));
        $book->inStock = false;
        $book->expected = $request->input('return_date');
        $book->save();

        return redirect()->route('books.index');
    }
}
