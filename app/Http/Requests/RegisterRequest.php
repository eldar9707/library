<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => 'bail|required|alpha|min:2|max:292',
            'surname' => 'bail|required|alpha|min:2|max:292',
            'patronymic' => 'bail|required|alpha|min:2|max:292',
            'address' => 'bail|required|alpha_dash|min:2|max:292',
            'passportId' => 'bail|required|numeric|unique:App\Models\User,passportId',
            'libraryId' => 'bail|required|unique:App\Models\User,libraryId',
            'password' => 'bail|required|confirmed|min:4',
        ];
    }
}
