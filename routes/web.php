<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [\App\Http\Controllers\BooksController::class, 'index'])
    ->name('books.index');

Route::get('/category{category}', [\App\Http\Controllers\BooksController::class, 'category'])
    ->name('books.category');

Route::get('/create/application{book}', [\App\Http\Controllers\ApplicationsController::class, 'create'])
    ->name('books.application');

Route::post('/store/application', [App\Http\Controllers\ApplicationsController::class, 'store'])
    ->name('application.store');

Route::get('/users/register', [App\Http\Controllers\UsersController::class, 'register'])
    ->name('users.register');

Route::get('/users/books', [App\Http\Controllers\UsersController::class, 'show'])
    ->name('users.books');

Route::post('/users', [App\Http\Controllers\UsersController::class, 'store'])
    ->name('users.store');

Route::get('/login', [App\Http\Controllers\Auth\SessionsController::class, 'create'])
    ->name('sessions.login');

Route::post('/login', [App\Http\Controllers\Auth\SessionsController::class, 'store'])
    ->name('sessions.store');

Route::delete('/logout', [App\Http\Controllers\Auth\SessionsController::class, 'destroy'])
    ->name('sessions.destroy');
