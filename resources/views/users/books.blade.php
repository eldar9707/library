@extends('layouts.app')

@section('content')

    <table class="table">
        <thead>
        <tr>
            <th scope="col">Book Name</th>
            <th scope="col">Date of Return</th>
            <th scope="col">Status</th>
        </tr>
        </thead>
        <tbody>
            @foreach($applications as $item)
                <tr>
                    <th scope="row">{{$item->book->name}}</th>
                    <td>{{$item->book->expected->format('d.m.Y')}}</td>
                    <td>
                        @if($item->return_date > \Carbon\Carbon::now())
                            <p class="text-info">Expected</p>
                        @elseif(\Carbon\Carbon::now() > $item->return_date)
                            <p class="text-danger">Expired</p>
                        @endif
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

@endsection
