@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-6 offset-3 align-self-center">
            @include('layouts.alerts')
            <h1 class="text-center">Log in</h1>
            <hr>
            <form action="{{route('sessions.store')}}" method="post">
                @csrf
                <div class="form-group">
                    <label for="exampleInputLibraryId1">Library ID</label>
                    <input type="text" class="form-control" id="exampleInputLibraryId1" name="libraryId">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Password</label>
                    <input type="password" class="form-control" id="exampleInputPassword1" name="password">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button> OR  <a href="{{route('users.register')}}">register</a>
            </form>
        </div>
    </div>
@endsection
