@extends('layouts.app')

@section('content')
    @include('layouts.alerts')
    <h2>All Books</h2>
    <div class="dropdown">
        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Categories
        </button>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            <a class="dropdown-item" href="{{route('books.index')}}">All</a>
            @foreach($categories as $category)
                <a class="dropdown-item" href="{{ route('books.category', ['category' => $category]) }}">{{$category->name}}</a>
            @endforeach
        </div>
    </div>
    <div class="row justify-content-md-center pb-1">
        <div class="col-md-auto">
            {{ $books->links('pagination::bootstrap-4') }}
        </div>
    </div>
    <div class="row row-cols-1 row-cols-md-3">
    @foreach($books as $book)
            <div class="card my-3" >
                <div class="row no-gutters">
                    <div class="col-md-4 my-auto">
                        <img src="{{asset('/storage/' . $book->img)}}" alt="..." style="max-width: 70px;">
                    </div>
                    <div class="col-md-8">
                        <div class="card-body">
                            <h5 class="card-title">{{ $book->author }}</h5>
                            <p class="card-text">"{{ $book->name }}"</p>
                            @if($book->inStock)
                                @if(auth_user())
                                <a class="btn btn-outline-success"
                                   href="{{ route('books.application', ['book' => $book]) }}">Get the Book</a>
                                @else
                                    <p class="text-success">In Stock</p>
                                @endif
                            @else
                                <p class="text-danger">Will be in stock: {{$book->expected->format('d.m.Y')}}</p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
    @endforeach
    </div>
    <div class="row justify-content-md-center p-5">
        <div class="col-md-auto">
            {{ $books->links('pagination::bootstrap-4') }}
        </div>
    </div>
@endsection
