@extends('layouts.app')

@section('content')
    @include('layouts.alerts')
    <form action="{{route('application.store')}}" method="post">
        <input type="hidden" value="{{$book->id}}" name="book_id">
        @csrf
        <div class="form-group">
            <label for="exampleInputLibraryId1">Book ID</label>
            <input disabled type="text" class="form-control" id="exampleInputLibraryId1" value="{{$book->id}}">
        </div>
        <div class="form-group">
            <label for="exampleInputAuthor1">Author</label>
            <input disabled type="text" class="form-control" id="exampleInputAuthor1" value="{{$book->author}}">
        </div>
        <div class="form-group">
            <label for="exampleInputBookName1">Book Name</label>
            <input disabled type="text" class="form-control" id="exampleInputBookName1" value="{{$book->name}}">
        </div>


        <div class="form-group">
            <label for="exampleInputLibraryId1">Library ID Card</label>
            <input type="text" class="form-control" id="exampleInputLibraryId1" name="libraryId">
        </div>
        <div class="form-group">
            <label for="exampleReturnDate1">Return Date</label>
            <input type="date" class="form-control" id="exampleReturnDate1" name="return_date">
        </div>
        <button type="submit" class="btn btn-outline-primary">Apply</button>
    </form>
@endsection
