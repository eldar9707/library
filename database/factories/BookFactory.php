<?php

namespace Database\Factories;

use App\Models\Book;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Storage;

class BookFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Book::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {

        return [
            'name' => $this->faker->firstNameMale(),
            'author' => $this->faker->name(),
            'img' => $this->getImage(rand(1, 7)),
            'inStock' => $inStock = $this->faker->boolean(),
            'expected' => $inStock ? null : $this->faker->dateTimeBetween('01-01-2020', '01-01-2022'),
            'category_id' => rand(1, 5),
        ];
    }


    private function getImage($img_number = 1):string
    {
        $path = storage_path() . "/seed_img/" . $img_number . ".jpg";
        $img_name = md5($path) . ".jpg";
        $resize = \Intervention\Image\Facades\Image::make($path)->encode('jpg');
        Storage::disk('public')->put('img/' . $img_name, $resize->__toString());

        return 'img/' . $img_name;
    }
}
