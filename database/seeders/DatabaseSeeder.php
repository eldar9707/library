<?php

namespace Database\Seeders;

use App\Models\Application;
use App\Models\Book;
use App\Models\Category;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         User::factory(10)->create();

         $categories = [
             'Action and Adventure',
             'Classics',
             'Detective and Mystery',
             'Fantasy',
             'Horror',
             ];

         for ($i =0; $i < count($categories); $i++) {
             Category::factory()->state([
                 'name' => $categories[$i],
             ])->create();
         }

         Book::factory(70)->create();

         for ($i = 1; $i < 15; $i++) {

             $user = User::find(rand(1, 10));
             $book = Book::all()->where('inStock', '==', false)->random();

             if (!Application::all()->contains('book_id', $book->id)) {
                 Application::factory()->state([
                     'libraryId' => $user->libraryId,
                     'book_id' => $book->id,
                     'user_id' => $user->id,
                     'return_date' => $book->expected,
                 ])->create();
             }
         }
    }
}
